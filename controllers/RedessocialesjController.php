<?php

namespace app\controllers;

use Yii;
use app\models\Redessocialesj;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RedessocialesjController implements the CRUD actions for Redessocialesj model.
 */
class RedessocialesjController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Redessocialesj models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Redessocialesj::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Redessocialesj model.
     * @param integer $codigo_jugador
     * @param string $redes_sociales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_jugador, $redes_sociales)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_jugador, $redes_sociales),
        ]);
    }

    /**
     * Creates a new Redessocialesj model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Redessocialesj();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_jugador' => $model->codigo_jugador, 'redes_sociales' => $model->redes_sociales]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Redessocialesj model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_jugador
     * @param string $redes_sociales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_jugador, $redes_sociales)
    {
        $model = $this->findModel($codigo_jugador, $redes_sociales);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_jugador' => $model->codigo_jugador, 'redes_sociales' => $model->redes_sociales]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Redessocialesj model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_jugador
     * @param string $redes_sociales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_jugador, $redes_sociales)
    {
        $this->findModel($codigo_jugador, $redes_sociales)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redessocialesj model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_jugador
     * @param string $redes_sociales
     * @return Redessocialesj the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_jugador, $redes_sociales)
    {
        if (($model = Redessocialesj::findOne(['codigo_jugador' => $codigo_jugador, 'redes_sociales' => $redes_sociales])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
