<?php

namespace app\controllers;

use Yii;
use app\models\Redessocialese;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RedessocialeseController implements the CRUD actions for Redessocialese model.
 */
class RedessocialeseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Redessocialese models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Redessocialese::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Redessocialese model.
     * @param integer $codigo_equipo
     * @param string $redes_sociales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_equipo, $redes_sociales)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_equipo, $redes_sociales),
        ]);
    }

    /**
     * Creates a new Redessocialese model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Redessocialese();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_equipo' => $model->codigo_equipo, 'redes_sociales' => $model->redes_sociales]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Redessocialese model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_equipo
     * @param string $redes_sociales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_equipo, $redes_sociales)
    {
        $model = $this->findModel($codigo_equipo, $redes_sociales);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_equipo' => $model->codigo_equipo, 'redes_sociales' => $model->redes_sociales]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Redessocialese model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_equipo
     * @param string $redes_sociales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_equipo, $redes_sociales)
    {
        $this->findModel($codigo_equipo, $redes_sociales)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redessocialese model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_equipo
     * @param string $redes_sociales
     * @return Redessocialese the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_equipo, $redes_sociales)
    {
        if (($model = Redessocialese::findOne(['codigo_equipo' => $codigo_equipo, 'redes_sociales' => $redes_sociales])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
