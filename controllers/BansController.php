<?php

namespace app\controllers;

use Yii;
use app\models\Bans;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BansController implements the CRUD actions for Bans model.
 */
class BansController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bans models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Bans::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bans model.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $bans
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_partido, $codigo_equipo, $bans)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_partido, $codigo_equipo, $bans),
        ]);
    }

    /**
     * Creates a new Bans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bans();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo, 'bans' => $model->bans]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Bans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $bans
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_partido, $codigo_equipo, $bans)
    {
        $model = $this->findModel($codigo_partido, $codigo_equipo, $bans);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo, 'bans' => $model->bans]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $bans
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_partido, $codigo_equipo, $bans)
    {
        $this->findModel($codigo_partido, $codigo_equipo, $bans)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $bans
     * @return Bans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_partido, $codigo_equipo, $bans)
    {
        if (($model = Bans::findOne(['codigo_partido' => $codigo_partido, 'codigo_equipo' => $codigo_equipo, 'bans' => $bans])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
