<?php

namespace app\controllers;

use Yii;
use app\models\Campeonesmasjugados;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CampeonesmasjugadosController implements the CRUD actions for Campeonesmasjugados model.
 */
class CampeonesmasjugadosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Campeonesmasjugados models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Campeonesmasjugados::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Campeonesmasjugados model.
     * @param integer $codigo_jugador
     * @param string $campeones_mas_jugados
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_jugador, $campeones_mas_jugados)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_jugador, $campeones_mas_jugados),
        ]);
    }

    /**
     * Creates a new Campeonesmasjugados model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Campeonesmasjugados();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_jugador' => $model->codigo_jugador, 'campeones_mas_jugados' => $model->campeones_mas_jugados]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Campeonesmasjugados model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_jugador
     * @param string $campeones_mas_jugados
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_jugador, $campeones_mas_jugados)
    {
        $model = $this->findModel($codigo_jugador, $campeones_mas_jugados);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_jugador' => $model->codigo_jugador, 'campeones_mas_jugados' => $model->campeones_mas_jugados]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Campeonesmasjugados model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_jugador
     * @param string $campeones_mas_jugados
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_jugador, $campeones_mas_jugados)
    {
        $this->findModel($codigo_jugador, $campeones_mas_jugados)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Campeonesmasjugados model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_jugador
     * @param string $campeones_mas_jugados
     * @return Campeonesmasjugados the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_jugador, $campeones_mas_jugados)
    {
        if (($model = Campeonesmasjugados::findOne(['codigo_jugador' => $codigo_jugador, 'campeones_mas_jugados' => $campeones_mas_jugados])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
