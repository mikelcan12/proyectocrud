<?php

namespace app\controllers;

use Yii;
use app\models\Patrocinadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PatrocinadoresController implements the CRUD actions for Patrocinadores model.
 */
class PatrocinadoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Patrocinadores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Patrocinadores::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patrocinadores model.
     * @param integer $codigo_patrocidor
     * @param integer $codigo_equipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_patrocidor, $codigo_equipo)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_patrocidor, $codigo_equipo),
        ]);
    }

    /**
     * Creates a new Patrocinadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patrocinadores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_patrocidor' => $model->codigo_patrocidor, 'codigo_equipo' => $model->codigo_equipo]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Patrocinadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_patrocidor
     * @param integer $codigo_equipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_patrocidor, $codigo_equipo)
    {
        $model = $this->findModel($codigo_patrocidor, $codigo_equipo);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_patrocidor' => $model->codigo_patrocidor, 'codigo_equipo' => $model->codigo_equipo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patrocinadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_patrocidor
     * @param integer $codigo_equipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_patrocidor, $codigo_equipo)
    {
        $this->findModel($codigo_patrocidor, $codigo_equipo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Patrocinadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_patrocidor
     * @param integer $codigo_equipo
     * @return Patrocinadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_patrocidor, $codigo_equipo)
    {
        if (($model = Patrocinadores::findOne(['codigo_patrocidor' => $codigo_patrocidor, 'codigo_equipo' => $codigo_equipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
