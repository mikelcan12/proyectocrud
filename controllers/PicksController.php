<?php

namespace app\controllers;

use Yii;
use app\models\Picks;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PicksController implements the CRUD actions for Picks model.
 */
class PicksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Picks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Picks::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Picks model.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $picks
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_partido, $codigo_equipo, $picks)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_partido, $codigo_equipo, $picks),
        ]);
    }

    /**
     * Creates a new Picks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Picks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo, 'picks' => $model->picks]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Picks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $picks
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_partido, $codigo_equipo, $picks)
    {
        $model = $this->findModel($codigo_partido, $codigo_equipo, $picks);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo, 'picks' => $model->picks]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Picks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $picks
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_partido, $codigo_equipo, $picks)
    {
        $this->findModel($codigo_partido, $codigo_equipo, $picks)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Picks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_partido
     * @param integer $codigo_equipo
     * @param string $picks
     * @return Picks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_partido, $codigo_equipo, $picks)
    {
        if (($model = Picks::findOne(['codigo_partido' => $codigo_partido, 'codigo_equipo' => $codigo_equipo, 'picks' => $picks])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
