<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campeonesmasjugados".
 *
 * @property int $codigo_jugador
 * @property string $campeones_mas_jugados
 *
 * @property Jugadores $codigoJugador
 */
class Campeonesmasjugados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campeonesmasjugados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_jugador', 'campeones_mas_jugados'], 'required'],
            [['codigo_jugador'], 'integer'],
            [['campeones_mas_jugados'], 'string', 'max' => 100],
            [['codigo_jugador', 'campeones_mas_jugados'], 'unique', 'targetAttribute' => ['codigo_jugador', 'campeones_mas_jugados']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugador' => 'Codigo Jugador',
            'campeones_mas_jugados' => 'Campeones Mas Jugados',
        ];
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
