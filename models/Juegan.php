<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $codigo_partido
 * @property int $codigo_equipo
 * @property int|null $heraldos_conseguidos
 * @property int|null $nº_asesinatos
 * @property int|null $barones_derrotados
 * @property int|null $dragones_obtenidos
 * @property int|null $inhibidores_destruidos
 * @property int|null $destruccion_nexo
 * @property int|null $torres_destruidas
 * @property int|null $oro_conseguido
 *
 * @property Bans[] $bans
 * @property Bans[] $bans0
 * @property Equipos $codigoEquipo
 * @property Partidos $codigoPartido
 * @property Picks[] $picks
 * @property Picks[] $picks0
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_equipo'], 'required'],
            [['codigo_partido', 'codigo_equipo', 'heraldos_conseguidos', 'nº_asesinatos', 'barones_derrotados', 'dragones_obtenidos', 'inhibidores_destruidos', 'destruccion_nexo', 'torres_destruidas', 'oro_conseguido'], 'integer'],
            [['codigo_partido', 'codigo_equipo'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_equipo']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo Partido',
            'codigo_equipo' => 'Codigo Equipo',
            'heraldos_conseguidos' => 'Heraldos Conseguidos',
            'nº_asesinatos' => 'Nº Asesinatos',
            'barones_derrotados' => 'Barones Derrotados',
            'dragones_obtenidos' => 'Dragones Obtenidos',
            'inhibidores_destruidos' => 'Inhibidores Destruidos',
            'destruccion_nexo' => 'Destruccion Nexo',
            'torres_destruidas' => 'Torres Destruidas',
            'oro_conseguido' => 'Oro Conseguido',
        ];
    }

    /**
     * Gets query for [[Bans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBans()
    {
        return $this->hasMany(Bans::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Bans0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBans0()
    {
        return $this->hasMany(Bans::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[Picks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPicks()
    {
        return $this->hasMany(Picks::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Picks0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPicks0()
    {
        return $this->hasMany(Picks::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
