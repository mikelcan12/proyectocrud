<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bans".
 *
 * @property int $codigo_partido
 * @property int $codigo_equipo
 * @property string $bans
 *
 * @property Juegan $codigoEquipo
 * @property Juegan $codigoPartido
 */
class Bans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_equipo', 'bans'], 'required'],
            [['codigo_partido', 'codigo_equipo'], 'integer'],
            [['bans'], 'string', 'max' => 100],
            [['codigo_partido', 'codigo_equipo', 'bans'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_equipo', 'bans']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Juegan::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Juegan::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo Partido',
            'codigo_equipo' => 'Codigo Equipo',
            'bans' => 'Bans',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Juegan::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Juegan::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
