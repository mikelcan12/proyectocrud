<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "redessocialese".
 *
 * @property int $codigo_equipo
 * @property string $redes_sociales
 *
 * @property Equipos $codigoEquipo
 */
class Redessocialese extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'redessocialese';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_equipo', 'redes_sociales'], 'required'],
            [['codigo_equipo'], 'integer'],
            [['redes_sociales'], 'string', 'max' => 100],
            [['codigo_equipo', 'redes_sociales'], 'unique', 'targetAttribute' => ['codigo_equipo', 'redes_sociales']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_equipo' => 'Codigo Equipo',
            'redes_sociales' => 'Redes Sociales',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }
}
