<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $codigo_partido
 * @property string|null $fase_jornada
 * @property string|null $fecha
 * @property string|null $hora
 *
 * @property Juegan[] $juegans
 * @property Equipos[] $codigoEquipos
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora'], 'safe'],
            [['fase_jornada'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo Partido',
            'fase_jornada' => 'Fase Jornada',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
        ];
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipos()
    {
        return $this->hasMany(Equipos::className(), ['codigo_equipo' => 'codigo_equipo'])->viaTable('juegan', ['codigo_partido' => 'codigo_partido']);
    }
}
