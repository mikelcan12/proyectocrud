<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "picks".
 *
 * @property int $codigo_partido
 * @property int $codigo_equipo
 * @property string $picks
 *
 * @property Juegan $codigoEquipo
 * @property Juegan $codigoPartido
 */
class Picks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'picks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_equipo', 'picks'], 'required'],
            [['codigo_partido', 'codigo_equipo'], 'integer'],
            [['picks'], 'string', 'max' => 100],
            [['codigo_partido', 'codigo_equipo', 'picks'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_equipo', 'picks']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Juegan::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Juegan::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo Partido',
            'codigo_equipo' => 'Codigo Equipo',
            'picks' => 'Picks',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Juegan::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Juegan::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
