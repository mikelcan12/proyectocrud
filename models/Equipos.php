<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $codigo_equipo
 * @property string|null $descripcion
 * @property string|null $nombre
 * @property int|null $numero_integrantes
 *
 * @property Entrenadores[] $entrenadores
 * @property Juegan[] $juegans
 * @property Partidos[] $codigoPartidos
 * @property Jugadores[] $jugadores
 * @property Patrocinadores[] $patrocinadores
 * @property Redessocialese[] $redessocialese
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['numero_integrantes'], 'integer'],
            [['nombre'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_equipo' => 'Codigo Equipo',
            'descripcion' => 'Descripcion',
            'nombre' => 'Nombre',
            'numero_integrantes' => 'Numero Integrantes',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_partido' => 'codigo_partido'])->viaTable('juegan', ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Patrocinadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinadores()
    {
        return $this->hasMany(Patrocinadores::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Redessocialese]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRedessocialese()
    {
        return $this->hasMany(Redessocialese::className(), ['codigo_equipo' => 'codigo_equipo']);
    }
}
