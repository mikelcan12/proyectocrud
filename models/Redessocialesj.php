<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "redessocialesj".
 *
 * @property int $codigo_jugador
 * @property string $redes_sociales
 *
 * @property Jugadores $codigoJugador
 */
class Redessocialesj extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'redessocialesj';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_jugador', 'redes_sociales'], 'required'],
            [['codigo_jugador'], 'integer'],
            [['redes_sociales'], 'string', 'max' => 100],
            [['codigo_jugador', 'redes_sociales'], 'unique', 'targetAttribute' => ['codigo_jugador', 'redes_sociales']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugador' => 'Codigo Jugador',
            'redes_sociales' => 'Redes Sociales',
        ];
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
