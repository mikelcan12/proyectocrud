<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $codigo_jugador
 * @property string|null $rol
 * @property string|null $nombre
 * @property string|null $nacionalidad
 * @property string|null $descripcion
 * @property string|null $nick
 * @property int|null $codigo_equipo
 *
 * @property Campeonesmasjugados[] $campeonesmasjugados
 * @property Equipos $codigoEquipo
 * @property Redessocialesj[] $redessocialesjs
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['codigo_equipo'], 'integer'],
            [['rol', 'nacionalidad', 'nick'], 'string', 'max' => 30],
            [['nombre'], 'string', 'max' => 25],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugador' => 'Codigo Jugador',
            'rol' => 'Rol',
            'nombre' => 'Nombre',
            'nacionalidad' => 'Nacionalidad',
            'descripcion' => 'Descripcion',
            'nick' => 'Nick',
            'codigo_equipo' => 'Codigo Equipo',
        ];
    }

    /**
     * Gets query for [[Campeonesmasjugados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampeonesmasjugados()
    {
        return $this->hasMany(Campeonesmasjugados::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Redessocialesjs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRedessocialesjs()
    {
        return $this->hasMany(Redessocialesj::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
