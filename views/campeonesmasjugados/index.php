<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campeonesmasjugados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campeonesmasjugados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Campeonesmasjugados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_jugador',
            'campeones_mas_jugados',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
