<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Campeonesmasjugados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campeonesmasjugados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_jugador')->textInput() ?>

    <?= $form->field($model, 'campeones_mas_jugados')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
