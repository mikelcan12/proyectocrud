<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Campeonesmasjugados */

$this->title = 'Update Campeonesmasjugados: ' . $model->codigo_jugador;
$this->params['breadcrumbs'][] = ['label' => 'Campeonesmasjugados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_jugador, 'url' => ['view', 'codigo_jugador' => $model->codigo_jugador, 'campeones_mas_jugados' => $model->campeones_mas_jugados]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="campeonesmasjugados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
