<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Campeonesmasjugados */

$this->title = $model->codigo_jugador;
$this->params['breadcrumbs'][] = ['label' => 'Campeonesmasjugados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="campeonesmasjugados-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_jugador' => $model->codigo_jugador, 'campeones_mas_jugados' => $model->campeones_mas_jugados], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_jugador' => $model->codigo_jugador, 'campeones_mas_jugados' => $model->campeones_mas_jugados], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_jugador',
            'campeones_mas_jugados',
        ],
    ]) ?>

</div>
