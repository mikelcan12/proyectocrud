<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Campeonesmasjugados */

$this->title = 'Create Campeonesmasjugados';
$this->params['breadcrumbs'][] = ['label' => 'Campeonesmasjugados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campeonesmasjugados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
