<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Redessocialese */

$this->title = $model->codigo_equipo;
$this->params['breadcrumbs'][] = ['label' => 'Redessocialese', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="redessocialese-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_equipo' => $model->codigo_equipo, 'redes_sociales' => $model->redes_sociales], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_equipo' => $model->codigo_equipo, 'redes_sociales' => $model->redes_sociales], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_equipo',
            'redes_sociales',
        ],
    ]) ?>

</div>
