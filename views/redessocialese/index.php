<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redessocialese';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redessocialese-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Redessocialese', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_equipo',
            'redes_sociales',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
