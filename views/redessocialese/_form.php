<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Redessocialese */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="redessocialese-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_equipo')->textInput() ?>

    <?= $form->field($model, 'redes_sociales')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
