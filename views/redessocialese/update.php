<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Redessocialese */

$this->title = 'Update Redessocialese: ' . $model->codigo_equipo;
$this->params['breadcrumbs'][] = ['label' => 'Redessocialese', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_equipo, 'url' => ['view', 'codigo_equipo' => $model->codigo_equipo, 'redes_sociales' => $model->redes_sociales]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="redessocialese-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
