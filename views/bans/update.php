<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bans */

$this->title = 'Update Bans: ' . $model->codigo_partido;
$this->params['breadcrumbs'][] = ['label' => 'Bans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_partido, 'url' => ['view', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo, 'bans' => $model->bans]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bans-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
