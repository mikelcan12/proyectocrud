<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bans */

$this->title = 'Create Bans';
$this->params['breadcrumbs'][] = ['label' => 'Bans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
