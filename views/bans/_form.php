<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_partido')->textInput() ?>

    <?= $form->field($model, 'codigo_equipo')->textInput() ?>

    <?= $form->field($model, 'bans')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
