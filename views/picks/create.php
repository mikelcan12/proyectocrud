<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Picks */

$this->title = 'Create Picks';
$this->params['breadcrumbs'][] = ['label' => 'Picks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="picks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
