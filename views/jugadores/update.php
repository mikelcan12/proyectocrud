<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */

$this->title = 'Update Jugadores: ' . $model->codigo_jugador;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_jugador, 'url' => ['view', 'id' => $model->codigo_jugador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jugadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
