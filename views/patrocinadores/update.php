<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */

$this->title = 'Update Patrocinadores: ' . $model->codigo_patrocidor;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_patrocidor, 'url' => ['view', 'codigo_patrocidor' => $model->codigo_patrocidor, 'codigo_equipo' => $model->codigo_equipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrocinadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
