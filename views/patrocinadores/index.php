<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patrocinadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Patrocinadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_patrocidor',
            'nombre',
            'codigo_equipo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
