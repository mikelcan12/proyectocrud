<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = $model->codigo_partido;
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="juegan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_partido',
            'codigo_equipo',
            'heraldos_conseguidos',
            'nº_asesinatos',
            'barones_derrotados',
            'dragones_obtenidos',
            'inhibidores_destruidos',
            'destruccion_nexo',
            'torres_destruidas',
            'oro_conseguido',
        ],
    ]) ?>

</div>
