<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = 'Update Juegan: ' . $model->codigo_partido;
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_partido, 'url' => ['view', 'codigo_partido' => $model->codigo_partido, 'codigo_equipo' => $model->codigo_equipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="juegan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
