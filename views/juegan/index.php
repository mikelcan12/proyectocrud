<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Juegans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Juegan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_partido',
            'codigo_equipo',
            'heraldos_conseguidos',
            'nº_asesinatos',
            'barones_derrotados',
            //'dragones_obtenidos',
            //'inhibidores_destruidos',
            //'destruccion_nexo',
            //'torres_destruidas',
            //'oro_conseguido',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
