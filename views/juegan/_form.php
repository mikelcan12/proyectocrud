<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_partido')->textInput() ?>

    <?= $form->field($model, 'codigo_equipo')->textInput() ?>

    <?= $form->field($model, 'heraldos_conseguidos')->textInput() ?>

    <?= $form->field($model, 'nº_asesinatos')->textInput() ?>

    <?= $form->field($model, 'barones_derrotados')->textInput() ?>

    <?= $form->field($model, 'dragones_obtenidos')->textInput() ?>

    <?= $form->field($model, 'inhibidores_destruidos')->textInput() ?>

    <?= $form->field($model, 'destruccion_nexo')->textInput() ?>

    <?= $form->field($model, 'torres_destruidas')->textInput() ?>

    <?= $form->field($model, 'oro_conseguido')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
