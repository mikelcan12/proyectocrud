<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Redessocialesj */

$this->title = 'Update Redessocialesj: ' . $model->codigo_jugador;
$this->params['breadcrumbs'][] = ['label' => 'Redessocialesjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_jugador, 'url' => ['view', 'codigo_jugador' => $model->codigo_jugador, 'redes_sociales' => $model->redes_sociales]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="redessocialesj-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
