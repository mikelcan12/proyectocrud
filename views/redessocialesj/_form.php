<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Redessocialesj */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="redessocialesj-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_jugador')->textInput() ?>

    <?= $form->field($model, 'redes_sociales')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
