<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Redessocialesj */

$this->title = 'Create Redessocialesj';
$this->params['breadcrumbs'][] = ['label' => 'Redessocialesjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redessocialesj-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
